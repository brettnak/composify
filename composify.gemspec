lib_path  = "./lib"
spec_path = "./spec"
bin_path  = "./bin"

libs  = Dir.glob("#{lib_path}/**/*.rb")
libs += Dir.glob("#{lib_path}/**/*.erb")

specs = Dir.glob("#{spec_path}/**/*.rb")
bins  = Dir.glob("#{bin_path}/**/*")

flist = libs + specs + bins
flist << "composify.gemspec"
flist << "README.md"

require_relative './lib/version'

summary = <<-EOD
A config generation library to generate a few Dockerfiles, Makefile, and
docker-compose.yml file that will get everything setup for local docker
development driven by make.  Specifically meant for use with non-trivial
multi-component development setups.  Also handy for trivial single container
development settings.
EOD

Gem::Specification.new do |gem|
  gem.name          = "composify"
  gem.version       = Composify::VERSION
  gem.authors       = ["Brett Nakashima"]
  gem.email         = ["brettnak@gmail.com"]
  gem.summary       = "Makefile driven docker-compose development environment."
  gem.description   = summary

  # TODO: Rename this repo to match the gem name
  gem.homepage      = "http://gitlab.com/brettnak/composify"

  gem.files         = flist
  gem.executables   = bins.map{ |f| File.basename(f) }
  gem.test_files    = specs
  gem.require_paths = ["lib/"]

  gem.add_development_dependency 'rspec', '~> 3.1'
  gem.add_development_dependency 'trollop', '~> 2.1'

  gem.license = "MIT"
end
