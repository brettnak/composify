Sanely generate the infrastructure needed for developing a multi-component service in Docker.

Generates the following tree:

```
<app root>
├── Dockerfile.base
├── Dockerfile.development # You'll install testing libraries and such in here.
├── Makefile
├── Makefile.internal
├── bin
│   ├── entrypoint.erb.sh
│   └── make # In the Dockerfile, symlinked to /usr/local/bin/make
└── docker-compose.yml
```

# Installation

* ruby > 2.1 is required.
* `gem install composify`

## Make Targets

* `make` - initialize everything, shortcut for
    - `pull`
    - `images`
    - `start_resources`
    - `setup`
    - `build`
* `make pull` - Pull all image dependencies
* `make images` - Build all images that need building
* `make setup` - run any setup or seeding that is needed.
* `make start` - start application containers
* `make stop` - stop application containers
* `make implode` - `docker-compose down --rmi all --volumes`
* `make shell` - Run a shell container
* `make production` - Build the non-development image
* `make build` - An example for how to pass-through to the `Makefile.internal` (TODO)

And, many more.  See the Makefile

## Development Workflow

### Initial

```shell
make && make start
```

### Code Iterations

1. Make your code changes
2. If using a compiled language, `make build` (or any pass-through target)
3. `make run`, or `make start` depending on how you wish to see output.

### Production (Non-Development) builds

Building the production image is pretty easy.  Although, instead of thinking of it as a 'production' image, think of it instead as a `non-development` image.  Basically, your production image just doesn't have some of the development and testing tools installed.  Otherwise, it's the same.  You can see this because the Dockerfile chain is `Dockerfile.base -> Dockerfile.development`.  The "production" image just doesn't have the development image as layers on top of the base image.

## Notes & Explainations

* The main thing going on here is that your main services in the `docker-compose.yml` file have a bind-mounted source directory.
* Once you run this project, all the generated files constitue the whole thing.

## TODO

1. Still need to work on the templates & generation if working on a compiled
project such as go, c, rust, etc.
2. Still need to add templates and such for projects like react that do live
recompilation and inotify/fswatch types of things.
3. Make a wizard command `composify init` instead of forcing all arguments to be present on the cli.
