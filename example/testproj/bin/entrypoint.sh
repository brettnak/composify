#!/bin/sh
set -e

cmd=$1; shift

case "$cmd" in
  shell)
    exec "/bin/bash"
    ;;
  console)
    # If your environment can have a console, put that target here.
    echo "No console configured for testproj"
    exit 0
    ;;
  pass)
    echo "Containter starts."
    exit 0
    ;;
  cmd)
    echo "$@"
    exec /bin/bash -c "$*"
    ;;
  setup)
    echo "This environment has no setup steps"
    exit 0
    ;;
  fullstack)
    # Put this components start command here.
    echo "No start command for fullstack"
    exit 0
    ;;
  *)
    echo "Please pass a valid command.  You passed '$cmd', which is invald."
    exit 3
esac
