This directory contains one folder, "testproj"

It's contents are the result of `cd`ing into that folder and running

```shell
composify \
  --base docker.io/ruby:2.5 \
  --components fullstack
```

It is all set to type `make` and have the full initialization process happen.
