require 'trollop'
require 'erb'
require 'fileutils'

require_relative './version'

module Composify
  class Template
    # TODO: Make these work on windows?
    FILE_LIST = {
      :entrypoint => './templates/bin/entrypoint.sh.erb',
      :docker_compose => './templates/docker-compose.yml.erb',
      :dockerfile_base => './templates/Dockerfile.base.erb',
      :dockerfile_production => './templates/Dockerfile.production.erb',
      :dockerfile_development => './templates/Dockerfile.development.erb',
      :makefile => './templates/Makefile.erb',
      # :makefile_internal => './templates/Makefile.internal.erb',
    }

    def initialize( file )
      @file = file
    end

    def destination
      return @_destination ||= @file.gsub( /^\.\/templates\//, './' ).gsub(/\.erb/, '')
    end

    # bind - the binding to use for ERB#result
    def result( bind )
      ERB.new(
        File.read( File.expand_path( File.join("..", @file), __FILE__ ) ),
        nil,
        '-'
      ).result( bind )
    end

    # bind - the binding to use for ERB#result
    def write( bind, force: false )
      if File.exists?( destination ) && !force
        STDERR.puts( "File already exists at #{destination}.  Refusing to overwrite" )
        return
      end

      if (dirname = File.dirname(destination)) != "."
        FileUtils.mkdir_p( dirname )
      end

      puts "Attempting to write #{destination}"
      File.open( destination, "w" ) do |f|
        contents = result( bind )
        f.write( contents )
      end
    end
  end

  class CLI
    def self.main( argv )
      cli = self.new( argv )
      cli.generate
    end

    def self.from_trollop( argv )
      opts = Trollop::options(argv) do
        version "composify #{Composify::VERSION}"
        banner "Generate a docker-development environment.\nNOTE: The name of your project is the directory"
        opt :base, "Base image of your application", :type => :string, :required => true
        opt :compilable, "Does your application code needs to be compiled?", :type => :bool, :default => false, :short => "C"
        opt :components, "The names of your application services, ie: api, scheduler, etc.", :type => :strings, :required => true
        opt :resource, "[ resource_name, resource_image ] The resources you will use.  May be specified more than once.", :type => :strings, :multi => true
        opt :force, "Overwrite any files that exist", :type => :bool, :default => false
      end

      opts[:resources] = opts[:resource].map{ |e| parse_resource(e) }

      opts[:root] = Dir.pwd
      opts[:project_name] = File.basename( opts[:root] ).gsub(/[^a-zA-Z0-9_]/, '')

      return opts
    end

    # definition is an array like this:
    # [ resource_name, image ]
    # Returns: [{ :name => "<resource_name>", :image => "<image_url>" }, ... ]
    def self.parse_resource( definition )
      return {
        :name  => definition[0],
        :image => definition[1]
      }
    end

    def initialize( argv )
      @argv = ARGV
    end

    def options
      return @_options ||= self.class.from_trollop( @argv )
    end

    def generate

      b = binding
      Template::FILE_LIST.each_pair do |_, file|
        template = Template.new( file )
        template.write( b, force: options[:force] )
      end
    end
  end

end
