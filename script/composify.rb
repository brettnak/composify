#!/usr/bin/env ruby

require_relative '../lib/composify'

if __FILE__ == $0
  Composify::CLI.main( ARGV )
end
